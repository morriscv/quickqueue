package network.marble.quickqueue.api;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import network.marble.inventoryapi.interfaces.ItemStackGetter;
import network.marble.inventoryapi.itemstacks.InventoryItem;

public class QQInviteToggleGetter implements ItemStackGetter{

	@Override
	public ItemStack getItemStack(InventoryItem inventoryItem, Player player) {
		if (QueueAPI.memberInvitingEnabled(QueueAPI.getPartyIDByLeaderID(QueueAPI.getMemberID(player.getUniqueId().toString())))){
			return QQMenus.toggleInvitingOnIS;
		}
		return QQMenus.toggleInvitingOffIS;
	}

}
