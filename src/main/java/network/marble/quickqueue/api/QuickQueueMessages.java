package network.marble.quickqueue.api;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.entity.Player;

import network.marble.bossbarmanager.api.BossBarManagerAPI;
import network.marble.inventoryapi.api.InventoryAPI;
import network.marble.messageapi.api.MessageExecutor;
import network.marble.quickqueue.QuickQueue;

public class QuickQueueMessages implements MessageExecutor{

	/***
	 * {@value 1} Reloads player inventory
	 */
	@Override
	public void executeMessage(String messageData) {
		if(messageData.startsWith("1:")){
			String targetUUID = messageData.substring(2);
			Player player = QuickQueue.getPlugin().getServer().getPlayer(UUID.fromString(targetUUID));
			if(player != null){
				QueueAPI.applyAppropriateInventoryToPlayer(player);
			}
		}else if(messageData.startsWith("2:")){
			String[] splitData = messageData.split(":");
			String targetUUID = splitData[1];
			int gameID = Integer.parseInt(splitData[2]);
			Player player = QuickQueue.getPlugin().getServer().getPlayer(UUID.fromString(targetUUID));
			if(player != null){
				ArrayList<Player> playerL = new ArrayList<Player>();
				playerL.add(player);
				BossBarManagerAPI.registerPlayers(playerL, QuickQueue.getPlugin().getName(), gameID);
			}
		}else if(messageData.startsWith("3:")){
			String targetUUID = messageData.substring(2);
			Player player = QuickQueue.getPlugin().getServer().getPlayer(UUID.fromString(targetUUID));
			if(player != null){
				ArrayList<Player> playerL = new ArrayList<Player>();
				playerL.add(player);
				BossBarManagerAPI.unregisterPlayers(playerL, QuickQueue.getPlugin().getName());
			}
		}else if(messageData.startsWith("4:")){
			String targetUUID = messageData.substring(2);
			Player player = QuickQueue.getPlugin().getServer().getPlayer(UUID.fromString(targetUUID));
			if(player != null){
				InventoryAPI.refreshPlayerView(player);
			}
		}
	}
}
