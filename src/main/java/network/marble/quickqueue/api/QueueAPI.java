package network.marble.quickqueue.api;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

import network.marble.bossbarmanager.api.BossBarManagerAPI;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.inventoryapi.api.InventoryAPI;
import network.marble.messageapi.api.MessageAPI;
import net.md_5.bungee.api.ChatColor;
import network.marble.quickqueue.QuickQueue;
import network.marble.serveridentifier.api.IdentifierAPI;
import redis.clients.jedis.Jedis;

public class QueueAPI {

	public static int getMemberID(String memberUUID){
		try {
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT memberID FROM members WHERE memberUUID = '" + memberUUID + "'");
			ResultSet rs = pst.executeQuery();
			
			int memberID = -1;
			if(rs.next()){
				memberID = rs.getInt("memberID");
			}
			rs.close();
			pst.close();
			return memberID;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public static int getMemberPartyID(int memberID){
		try {
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT partyID FROM members WHERE memberID = '" + memberID + "'");
			ResultSet rs = pst.executeQuery();
			
			int partyID = -1;
			if(rs.next()){
				partyID = rs.getInt("partyID");
			}
			rs.close();
			pst.close();
			return partyID;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public static String getMinigameName(int gameID){
		try {
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT minigameName FROM minigames WHERE minigameID = '" + gameID + "'");
			ResultSet rs = pst.executeQuery();
			
			String partyID = "";
			if(rs.next()){
				partyID = rs.getString("minigameName");
			}
			rs.close();
			pst.close();
			return partyID;
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Null name outputted to recover.");
			return "";
		}
	}
	
	public static int getQueuedMinigame(int partyID){
		try {
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT queues.minigameID FROM queues LEFT JOIN parties ON parties.queueID=queues.queueID WHERE parties.partyID = '" + partyID + "'");
			ResultSet rs = pst.executeQuery();
			
			int minigameID = -1;
			if(rs.next()){
				minigameID = rs.getInt("queues.minigameID");
			}
			rs.close();
			pst.close();
			return minigameID;
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Null name outputted to recover.");
			return -1;
		}
	}
	
	public static int getPartyIDByLeaderID(int memberID){
		try {
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT partyID FROM parties WHERE leaderID = '" + memberID + "'");
			ResultSet rs = pst.executeQuery();
			int partyID = -1;
			if(rs.next()){
				partyID = rs.getInt("partyID");
			}
			rs.close();
			pst.close();
			return partyID;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public static Map<Integer, String> getInvites(String target){
    	try{
    		PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT inviteID,inviteSenderID FROM invites WHERE inviteRecipientUUID = '" + target + "'");
    		ResultSet rs = pst.executeQuery();
    		
    		Map<Integer, String> invites = new HashMap<Integer, String>();
    		
    		while(rs.next()){
    			invites.put(rs.getInt("inviteID"), rs.getString("inviteSenderID"));
    		}
    		
    		pst.close();
    		rs.close();
    		return invites;
    	}catch(Exception e){
    		e.printStackTrace();
    		return null;
    	}
    }
	
	public static boolean inviteExists(String targetUUID, String sourceUUID){
    	boolean exists = false;
    	int senderPartyID = getMemberPartyID(getMemberID(sourceUUID));
    	try{
    		PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT inviteID FROM invites WHERE inviteRecipientUUID = '" + targetUUID + "' AND partyID = '" + senderPartyID + "'");
    		ResultSet rs = pst.executeQuery();
    		if(rs.next()){
    			exists = true;
    		}
    		pst.close();
    		rs.close();
    	}catch(Exception e){
    		e.printStackTrace();
    		return false;
    	}
    	return exists;
    }

	public static boolean isMinigameAccessible(int gameID){
		try {
			boolean accessible = false;
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT isPublic FROM minigames WHERE minigameID = '" + gameID + "'");
			ResultSet rs = pst.executeQuery();
			
			if(rs.next()){
				accessible = rs.getBoolean("isPublic");
			}
			rs.close();
			pst.close();
			return accessible;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean isMinigameQueueable(int gameID){
		try {
			boolean accessible = false;
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT isQueueable FROM minigames WHERE minigameID = '" + gameID + "'");
			ResultSet rs = pst.executeQuery();
			
			if(rs.next()){
				accessible = rs.getBoolean("isQueueable");
			}
			rs.close();
			pst.close();
			return accessible;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean checkMinigameInBeta(int gameID, int partyID){//TODO reduce to single statement
		try {
			boolean accessible = true;
			boolean inBeta = false;
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT isInBeta FROM minigames WHERE minigameID = '" + gameID + "'");
			ResultSet rs = pst.executeQuery();
			
			if(rs.next()){
				inBeta = rs.getBoolean("isInBeta");
			}
			rs.close();
			pst.close();
			if(inBeta){
				pst = QuickQueue.conn.prepareStatement("SELECT memberUUID FROM members WHERE partyID = '" + partyID + "'");
				rs = pst.executeQuery();
				
				while(rs.next()){
					if(!checkPlayerIsPriority(rs.getString("memberUUID"))){
						accessible = false;
						break;
					}
				}
			}
			
			pst.close();
			return accessible;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean isPartyQueued(int partyID){
		try {
			boolean isQueued = false;
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT isQueueing FROM parties WHERE partyID = '" + partyID + "'");
			ResultSet rs = pst.executeQuery();
			
			if(rs.next()){
				isQueued = rs.getBoolean("isQueueing");
			}
			rs.close();
			pst.close();
			return isQueued;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static void applyAppropriateInventoryToPlayer(Player player){
		String playerUUID = player.getUniqueId().toString();
		int partyID = getMemberPartyID(getMemberID(playerUUID));
		boolean memberInvitingEnabled = memberInvitingEnabled(partyID);
		if(isPlayerPartyLeader(playerUUID)){
			if(memberInvitingEnabled){
				InventoryAPI.setPlayerInventoryType(player, 4);
			}else{
				InventoryAPI.setPlayerInventoryType(player, 1);
			}
		}else if(partyID != -1){
			if(memberInvitingEnabled){
				InventoryAPI.setPlayerInventoryType(player, 3);
			}else{
				InventoryAPI.setPlayerInventoryType(player, 2);
			}
		}else{
			InventoryAPI.setPlayerInventoryType(player, 0);
		}
	}
	
	public static ArrayList<String> getPartyMembers(int partyID){
		ArrayList<String> members = new ArrayList<String>();
		
		try {
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT memberUUID FROM members WHERE partyID = '" + partyID + "'");
			ResultSet rs = pst.executeQuery();
			
			try (Jedis jedis = QuickQueue.pool.getResource()){
				while(rs.next()){
					String memberUUID = rs.getString("memberUUID");
					members.add(memberUUID);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
		return members;
	}
	
	public static String[] messageParty(int partyID, String message, boolean isCommand){
		List<String> members = new ArrayList<String>();
		
		try {
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT memberUUID FROM members WHERE partyID = '" + partyID + "'");
			ResultSet rs = pst.executeQuery();
			
			try (Jedis jedis = QuickQueue.pool.getResource()){
				while(rs.next()){
					String memberUUID = rs.getString("memberUUID");
					members.add(memberUUID);
					if(IdentifierAPI.getPlayerServerID(memberUUID) == IdentifierAPI.getServerID()){
						Player player = QuickQueue.getPlugin().getServer().getPlayer(UUID.fromString(memberUUID));
						if(player != null){
							if(!isCommand){
								player.sendMessage(message);
							}else{
								QuickQueue.getMessageClass().executeMessage(message+memberUUID);
							}
						}
					}else{
						if(isCommand){
							MessageAPI.sendMessage(String.valueOf(IdentifierAPI.getPlayerServerID(memberUUID)), QuickQueue.getPlugin().getName(), false, message+memberUUID);
						}else{
							MessageAPI.sendMessage(String.valueOf(IdentifierAPI.getPlayerServerID(memberUUID)), memberUUID, true, message);
						}
					}
				}
			}
			
			rs.close();
			pst.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String[] arrayUUIDs = members.toArray(new String[members.size()]);
		return arrayUUIDs;
	}

	public static String[] messagePartyExcludeSender(int partyID, String message, boolean isCommand, String ignoredUUID){
		List<String> members = new ArrayList<String>();
		try {
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT memberUUID FROM members WHERE partyID = '" + partyID + "'");
			ResultSet rs = pst.executeQuery();
			

			while(rs.next()){
				String uuid = rs.getString("memberUUID");
				members.add(uuid);
				
				if(!uuid.equals(ignoredUUID)){
					if(isCommand){
						MessageAPI.sendMessage(String.valueOf(IdentifierAPI.getPlayerServerID(uuid)), QuickQueue.getPlugin().getName(), false, message+uuid);
					}else{
						MessageAPI.sendMessage(String.valueOf(IdentifierAPI.getPlayerServerID(uuid)), uuid, true, message);
					}
				}
			}
			
			rs.close();
			pst.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String[] arrayUUIDs = members.toArray(new String[members.size()]);
		return arrayUUIDs;
	}
	
	public static void messageUUIDList(String[] uuids, String message, boolean isCommand){
		for(String uuid: uuids){
			if(isCommand){
				MessageAPI.sendMessage(String.valueOf(IdentifierAPI.getPlayerServerID(uuid)), QuickQueue.getPlugin().getName(), false, message+uuid);
			}else{
				MessageAPI.sendMessage(String.valueOf(IdentifierAPI.getPlayerServerID(uuid)), uuid, true, message);
			}
		}
	}
	
	public static void messageUUID(String uuid, String message, boolean isCommand){
		if(isCommand){
			MessageAPI.sendMessage(String.valueOf(IdentifierAPI.getPlayerServerID(uuid)), QuickQueue.getPlugin().getName(), false, message+uuid);
		}else{
			MessageAPI.sendMessage(String.valueOf(IdentifierAPI.getPlayerServerID(uuid)), uuid, true, message);
		}
	}
	
	public static boolean memberInvitingEnabled(int partyID){
		try {
			boolean invitingEnabled = false;
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT memberInviting FROM parties WHERE partyID = '" + partyID + "'");
			ResultSet rs = pst.executeQuery();
			
			if(rs.next()){
				invitingEnabled = rs.getBoolean("memberInviting");
			}
			rs.close();
			pst.close();
			return invitingEnabled;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static String getPartyLeaderUUID(int partyID){
		try {
			String leaderUUID = "";
			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT members.memberUUID FROM `parties` LEFT JOIN `members` ON members.memberID=parties.leaderID WHERE parties.partyID='" + partyID + "'");
			ResultSet rs = pst.executeQuery();
			
			if(rs.next()){
				leaderUUID = rs.getString("memberUUID");
			}
			rs.close();
			pst.close();
			return leaderUUID;
		} catch (SQLException e) {
			e.printStackTrace();
			return "";
		}
	}
	

    public static boolean checkPlayerIsPriority(String playerUUID){
    	return DataAPI.isPriority(DataAPI.getUser(playerUUID).getRank());
    }
    
    public static boolean destroyInvites(String targetUUID){
    	try{
    		PreparedStatement pst = QuickQueue.conn.prepareStatement("DELETE FROM invites WHERE inviteRecipientUUID = '" + targetUUID + "'");
    		pst.executeUpdate();
    		pst.close();
    		return true;
    	}catch(Exception e){
    		e.printStackTrace();
    		return false;
    	}
    }
	
    public static boolean isPlayerOnline(String uuid){
    	boolean isOnline = false;
    	try (Jedis jedis = QuickQueue.pool.getResource()){
		    isOnline = jedis.exists("player:" + uuid);
		}
    	return isOnline;
    }
    
    public static void removeListFromQueueBossBars(ArrayList<String> uuids){
    	ArrayList<Player> onlineMembers = new ArrayList<Player>();
		for(String s:uuids){
			UUID uuid = UUID.fromString(s);
			Player player = QuickQueue.getPlugin().getServer().getPlayer(uuid);
			if(player!=null){
				onlineMembers.add(player);
			}else{
				QueueAPI.messageUUID(s, "3:" + s, true);
			}
			
			
		}
		BossBarManagerAPI.unregisterPlayers(onlineMembers, QuickQueue.getPlugin().getName());
    }
    
    public static void addListToQueueBossBars(String[] uuids, int gameID){
    	ArrayList<Player> onlineMembers = new ArrayList<Player>();
		for(String s:uuids){
			UUID uuid = UUID.fromString(s);
			Player player = QuickQueue.getPlugin().getServer().getPlayer(uuid);
			if(player!=null){
				onlineMembers.add(player);
			}else{
				QueueAPI.messageUUID(s, "2:" + s + ":" + gameID, true);
			}
		}
		BossBarManagerAPI.registerPlayers(onlineMembers, QuickQueue.getPlugin().getName(), gameID);
    }
    
    public static void addUUIDToQueueBossBars(String uuid, int gameID){
    	String[] arrayUUID = new String[1];
    	arrayUUID[0] = uuid;
    	addListToQueueBossBars(arrayUUID, gameID);
    }
    
    public static boolean destroyParty(int partyID, String leaderUUID){
    	boolean failed = false;
    	String[] members = null;
    	try{
			members = messagePartyExcludeSender(partyID, ChatColor.GREEN + "Your party was disbanded.", false, leaderUUID);
			ArrayList<String> membersList = new ArrayList<String>();
			for(String s: members){
				membersList.add(s);
			}
			membersList.add(leaderUUID);
			removeListFromQueueBossBars(membersList);
			
			PreparedStatement pst = QuickQueue.conn.prepareStatement("DELETE FROM parties WHERE partyID = '" + partyID + "'");
			pst.executeUpdate();
			pst.close();
		}catch(Exception e){
    		e.printStackTrace();
    		failed = true;
    	}
    	
    	try{
    		messageUUIDList(members, "1:", true);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	if(failed){
    		try{
    			messageParty(partyID, ChatColor.RED + "An error occurred while disbanding this party. Disbandment has been cancelled. Relogging is recommended should you expierience any issues.", false);
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    	}
    	return !failed;
    }
    
    public static boolean removePlayerFromParty(int memberID){
    	try{
	    	PreparedStatement pst = QuickQueue.conn.prepareStatement("DELETE FROM members WHERE memberID = '" + memberID + "'");
			pst.executeUpdate();
			pst.close();
    	}catch(Exception e){
			e.printStackTrace();
			return false;
		}
    	return true;
    }
    
    public static boolean isPlayerPartyLeader(String senderUUID){
    	int partyID = getPartyIDByLeaderID(getMemberID(senderUUID));
		if(partyID != -1){//check sender is leader based on prior check
			return true;
		}
		return false;
    }
    
    public UUID[] getPlayersInParty(String memberUUID){
    	int partyID = getMemberPartyID(getMemberID(memberUUID));
    	List<UUID> uuids = new ArrayList<UUID>();
    	
    	if(partyID != -1){
    		try{
    			
    			PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT * FROM members WHERE partyID = '" + partyID + "'");
    			ResultSet rs = pst.executeQuery();
    			
    			if(rs.next()){
    				uuids.add(UUID.fromString(rs.getString("memberUUID")));
    			}
    			rs.close();
    			pst.close();
    		}catch(Exception e){
        		e.printStackTrace();
        		return null;
        	}
    	}
    	UUID[] arrayUUIDs = uuids.toArray(new UUID[uuids.size()]);
		return arrayUUIDs;
    }
    
    public static void markPlayerOffline(Player p){
		String playerUUID = p.getUniqueId().toString();
		
		ArrayList<String> member = new ArrayList<String>();
		member.add(playerUUID);
		removeListFromQueueBossBars(member);
		
		boolean handleFailiure = false;
		
		try{
			int memberID = getMemberID(playerUUID);
			int partyID = getPartyIDByLeaderID(memberID);
			
			if(memberID != -1){//Check sender is in a party
				if(partyID != -1){//Is sender leader of said party?
					destroyParty(partyID, playerUUID);
				}else{
					removePlayerFromParty(memberID);
				}
			}else{
				destroyInvites(playerUUID);
			}
		}catch(Exception e){
			e.printStackTrace();
			handleFailiure = true;
		}
		
		if(handleFailiure){
			//TODO jedis log error for later external correction attempts
		}
		
	}
}
