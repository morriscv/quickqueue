package network.marble.quickqueue.api;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import network.marble.inventoryapi.api.InventoryAPI;
import network.marble.inventoryapi.itemstacks.ActionItemStack;
import network.marble.inventoryapi.itemstacks.InventoryItem;
import net.md_5.bungee.api.ChatColor;
import network.marble.quickqueue.QuickQueue;

public class QQMenus {
	public static int noPartyMenu = 0;//modifying default
	public static int leaderMenu;//1
	public static int memberMenu;//2
	public static int memberToggledMenu;//3
	public static int leaderToggledMenu;//4
	public static ItemStack toggleInvitingOnIS;//Green potion to display that member inviting is enabled
	public static ItemStack toggleInvitingOffIS;//Red potion to display that member inviting is disabled
	
	public static void buildInventoryMenus(){
		//Friendly names for sql statements
    	String invitesSentToUUID = "SELECT members.memberUUID FROM invites INNER JOIN parties ON invites.PartyID = parties.partyID INNER JOIN members ON parties.leaderID = members.memberID WHERE inviteRecipientUUID = '$ORIGIN'";
    	String invitesSentFromLeaderUUID = "SELECT invites.inviteRecipientUUID FROM members INNER JOIN parties ON members.memberID = parties.leaderID INNER JOIN invites ON invites.PartyID = parties.partyID WHERE memberUUID = '$ORIGIN'";
    	String membersOfLeaderUUIDParty = "SELECT m.memberUUID FROM members INNER JOIN parties on parties.leaderID = members.memberID INNER JOIN members m ON parties.partyID = m.partyID WHERE members.memberUUID = '$ORIGIN' AND m.memberUUID != members.memberUUID";
    	String onlinePlayers = "players";
    	
    	//Create Inventories in API
    	//noPartyMenu will use the API's default inventory
    	leaderMenu = InventoryAPI.createNewInventory();
    	memberMenu = InventoryAPI.createNewInventory();
    	
    	InventoryItem[] mainMenu = new InventoryItem[27];
    	
    	try{
		    PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT minigameID, minigameName, description, itemMaterial, metaData FROM minigames WHERE isPublic = '1' AND isQueueable = '1'");
		    ResultSet rs = pst.executeQuery();
		    int i = 0;
		    while(rs.next()){
		    	ItemStack is = new ItemStack(Material.getMaterial(rs.getString("itemMaterial")),1,rs.getShort("metaData"));
		    	ItemMeta meta = is.getItemMeta();
		    	meta.setDisplayName(ChatColor.GOLD + rs.getString("minigameName"));
		    	
		    	String loreRaw = rs.getString("description");
		    	String[] loreSplit= loreRaw.split(" ");
		    	List<String> lore = new ArrayList<String>();
		    	String loreLine = "";
		    	
		    	String colourSet = "5";
		    	for(int j = 0; j < loreSplit.length; ++j){
			    	String[] colours = loreSplit[j].split("��");
	    			
	    			if(colours.length != 1){
	    				colourSet = colours[colours.length-1].substring(0, 1);
	    			}
	    			
	    			if (j != loreSplit.length-1){
	    				loreSplit[j+1] = "��" + colourSet + loreSplit[j+1];
	    			}
		    	}
		    	
		    	for(int k = 0; k < loreSplit.length; ++k){
		    		if((ChatColor.stripColor((loreLine+loreSplit[k]))).length() < 30){
		    			loreLine += loreSplit[k] + " ";
		    		}else{
		    			lore.add(loreLine);
		    			loreLine = loreSplit[k] + " ";
		    		}
		    		
		    		if(k == loreSplit.length - 1){
		    			lore.add(loreSplit[k]);
		    		}
		    	}
		    	meta.setLore(lore);
		    	is.setItemMeta(meta);
		    	
		    	mainMenu[i] = new ActionItemStack(is, "qq joinqueue " + rs.getString("minigameID"), false);//"qq joinqueue $GAMEID"
		    	
		    	i++;
		    }
		    
		    pst.close();	
		    rs.close();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	ItemStack createPartyIS = new ItemStack(Material.ANVIL, 1, (short)0);//qq addparty
    	ItemMeta createPartyMeta = createPartyIS.getItemMeta();
    	createPartyMeta.setDisplayName(ChatColor.GOLD + "Create Party");
    	createPartyIS.setItemMeta(createPartyMeta);
    	
    	ItemStack viewInvitesIS = new ItemStack(Material.SKULL_ITEM, 1, (short)0);//qq acceptInvite | As a list of all pending
    	ItemMeta viewInvitesMeta = viewInvitesIS.getItemMeta();
    	viewInvitesMeta.setDisplayName(ChatColor.GOLD + "View Invites");
    	viewInvitesIS.setItemMeta(viewInvitesMeta);
    	
    	ItemStack joinQueueIS = new ItemStack(Material.FIREWORK, 1, (short)0);//qq joinqueue
    	ItemMeta joinQueueMeta = joinQueueIS.getItemMeta();
    	joinQueueMeta.setDisplayName(ChatColor.GOLD + "Join Minigame Queue");
    	joinQueueIS.setItemMeta(joinQueueMeta);
    	
    	ItemStack inviteMemberIS = new ItemStack(Material.BOOK, 1, (short)0);//qq invitemember
    	ItemMeta inviteMemberMeta = inviteMemberIS.getItemMeta();
    	inviteMemberMeta.setDisplayName(ChatColor.GOLD + "Invite Members");
    	inviteMemberIS.setItemMeta(inviteMemberMeta);
    	
    	ItemStack revokeInviteIS = new ItemStack(Material.WOOD_AXE, 1, (short)0);//qq revokeinvite
    	ItemMeta revokeInviteMeta = revokeInviteIS.getItemMeta();
    	revokeInviteMeta.setDisplayName(ChatColor.GOLD + "Cancel Invites");
    	revokeInviteMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
    	revokeInviteIS.setItemMeta(revokeInviteMeta);
    	
    	ItemStack removeMemberIS = new ItemStack(Material.TRAP_DOOR, 1, (short)0);//qq removemember
    	ItemMeta removeMemberMeta = removeMemberIS.getItemMeta();
    	removeMemberMeta.setDisplayName(ChatColor.GOLD + "Remove Members");
    	removeMemberIS.setItemMeta(removeMemberMeta);
    	
    	
    	toggleInvitingOnIS = new ItemStack(Material.POTION, 1, (short)0);//qq toggleInvitingMeta
    	PotionMeta pMOn = (PotionMeta)toggleInvitingOnIS.getItemMeta();
    	PotionData pDOn = new PotionData(PotionType.JUMP);
    	pMOn.setBasePotionData(pDOn);
    	pMOn.setDisplayName(ChatColor.GOLD + "Disable Member Inviting");
    	pMOn.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
    	toggleInvitingOnIS.setItemMeta(pMOn);
    	
    	toggleInvitingOffIS = new ItemStack(Material.POTION, 1, (short)0);//qq toggleInvitingMeta
    	PotionMeta pMOff = (PotionMeta)toggleInvitingOffIS.getItemMeta();
    	PotionData pDOff = new PotionData(PotionType.INSTANT_HEAL);
    	pMOff.setBasePotionData(pDOff);
    	pMOff.setDisplayName(ChatColor.GOLD + "Enable Member Inviting");
    	pMOff.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
    	toggleInvitingOffIS.setItemMeta(pMOff);
    	
    	ItemStack transferLeaderIS = new ItemStack(Material.POWERED_RAIL, 1, (short)0);//qq transferleadership
    	ItemMeta transferLeaderMeta = transferLeaderIS.getItemMeta();
    	transferLeaderMeta.setDisplayName(ChatColor.GOLD + "Transfer Leadership");
    	transferLeaderIS.setItemMeta(transferLeaderMeta);
    	
    	ItemStack disbandPartyIS = new ItemStack(Material.TNT, 1, (short)0);//qq removeparty
    	ItemMeta disbandPartyMeta = disbandPartyIS.getItemMeta();
    	disbandPartyMeta.setDisplayName(ChatColor.GOLD + "Disband Party");
    	disbandPartyIS.setItemMeta(disbandPartyMeta);
    	
    	ItemStack exitQueueIS = new ItemStack(Material.WOOD_DOOR, 1, (short)0);//qq exitqueue
    	ItemMeta exitQueueMeta = exitQueueIS.getItemMeta();
    	exitQueueMeta.setDisplayName(ChatColor.GOLD + "Leave Minigame Queue");
    	exitQueueIS.setItemMeta(exitQueueMeta);
    	
    	ItemStack leavePartyIS = new ItemStack(Material.SHEARS, 1, (short)0);//qq leaveparty
    	ItemMeta leavePartyMeta = leavePartyIS.getItemMeta();
    	leavePartyMeta.setDisplayName(ChatColor.GREEN + "Leave Party");
    	leavePartyIS.setItemMeta(leavePartyMeta);
    	
    	//No party
    	InventoryAPI.addItemToInventory(noPartyMenu, createPartyIS, 1, 4, "qq addparty", false);
    	InventoryAPI.addItemToInventory(noPartyMenu, viewInvitesIS, 9, 4, "qq acceptinvite $UUID", "\\$UUID", invitesSentToUUID, "memberUUID", true, false);
    	
    	//Leader
    	InventoryAPI.addItemToInventory(leaderMenu, joinQueueIS, 1, 4, 27, mainMenu, "Select A Minigame");
    	InventoryAPI.addItemToInventory(leaderMenu, inviteMemberIS, 2, 4, "qq invitemember $UUID", "\\$UUID", onlinePlayers, true, false);
    	InventoryAPI.addItemToInventory(leaderMenu, revokeInviteIS, 3, 4, "qq revokeinvite $UUID", "\\$UUID", invitesSentFromLeaderUUID, "inviteRecipientUUID", true, false);
    	InventoryAPI.addItemToInventory(leaderMenu, removeMemberIS, 4, 4, "qq removemember $UUID", "\\$UUID", membersOfLeaderUUIDParty, "memberUUID", true, false);
    	InventoryAPI.addItemToInventory(leaderMenu, transferLeaderIS, 7, 4, "qq transferleadership $UUID", "\\$UUID", membersOfLeaderUUIDParty, "memberUUID", true, true);
    	InventoryAPI.addItemToInventory(leaderMenu, disbandPartyIS, 8, 4, "qq removeparty", true);
    	InventoryAPI.addItemToInventory(leaderMenu, exitQueueIS, 9, 4, "qq exitqueue", true);
    	
    	//Member
    	InventoryAPI.addItemToInventory(memberMenu, leavePartyIS, 9, 4, "qq leaveparty", true);
    	
    	//Member of party with member inviting enabled
    	memberToggledMenu = InventoryAPI.duplicateInventory(memberMenu);
    	InventoryAPI.addItemToInventory(memberToggledMenu, inviteMemberIS, 1, 4, "qq invitemember $UUID", "\\$UUID", onlinePlayers, true, false);
    	
    	//Itemstacks for toggling of inventory state
    	//leaderToggledMenu = InventoryAPI.duplicateInventory(leaderMenu);
    	//InventoryAPI.addItemToInventory(leaderToggledMenu, toggleInvitingOnIS, 6, 4, "qq togglememberinviting", false);
    	InventoryAPI.addItemToInventory(leaderMenu, toggleInvitingOffIS, 6, 4, "qq togglememberinviting", false, new QQInviteToggleGetter());
    }
}
