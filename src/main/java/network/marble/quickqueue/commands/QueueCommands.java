package network.marble.quickqueue.commands;

import network.marble.badgeapi.api.BadgeAPI;
import network.marble.inventoryapi.api.InventoryAPI;

import net.md_5.bungee.api.ChatColor;
import network.marble.quickqueue.QuickQueue;
import network.marble.quickqueue.api.QueueAPI;
import network.marble.serveridentifier.api.IdentifierAPI;
import redis.clients.jedis.Jedis;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;
//TODO thread safety
//TODO proper error logging
//TODO allow instant join of minigame queue without creating party
public class QueueCommands implements CommandExecutor{
	
	public QueueCommands() {
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		boolean commandSuccess = false;
		if (cmd.getName().equalsIgnoreCase("qq")) {
			if (sender instanceof Player) {
				if(args.length > 0 && args.length <= 2){
					Player player = (Player) sender;
					String subCmd = args[0].toLowerCase();
					commandSuccess = true;
					switch(subCmd){
		           		case "addparty": 				addParty(player); break;
		           		case "removeparty": 			removeParty(player); break;
		           		case "leaveparty": 				leaveParty(player); break;
		           		case "removemember": 			removeMember(player, args); break;
		           		case "invitemember": 			inviteMember(player, args); break;
		           		case "transferleadership": 		transferLeadership(player, args); break;
		           		case "revokeinvite": 			revokeInvite(player, args); break;
		           		case "acceptinvite": 			acceptInvite(player, args); break;
		           		case "togglememberinviting": 	toggleMemberInviting(player); break;
		           		case "joinqueue": 				joinQueue(player, args); break;
		           		case "exitqueue": 				exitQueue(player); break;
		           		default: sender.sendMessage(ChatColor.RED + "Invalid action."); commandSuccess = false; break;
					}
				}
			}else{
	           sender.sendMessage("You must be a player to operate the queue!");
	           return false;
			}
		}
		return commandSuccess;
	}
	
    public void addParty(Player sender) {
    	String senderUUID = sender.getUniqueId().toString();
		
    	try{
    		
    		int senderID = QueueAPI.getMemberID(senderUUID);
    		if(senderID == -1){
    			PreparedStatement pst;
    			int partyID = -1;
    			pst = QuickQueue.conn.prepareStatement("INSERT INTO parties (queueID, isQueueing, memberInviting) VALUES (NULL, '0', '0')", Statement.RETURN_GENERATED_KEYS);
    			pst.executeUpdate();
    			ResultSet rs = pst.getGeneratedKeys();
    			if(rs.next()){
    				partyID = rs.getInt(1);
    			}
    			rs.close();
    			pst.close();
    			if(partyID != -1){
	    			int leaderID = -1;
	    			pst = QuickQueue.conn.prepareStatement("INSERT INTO members (memberUUID, partyID) VALUES ('" + senderUUID + "', '" + partyID + "')", Statement.RETURN_GENERATED_KEYS);
	    			pst.executeUpdate();
	    			rs = pst.getGeneratedKeys();
	    			if(rs.next()){
	    				leaderID = rs.getInt(1);
	    			}
	    			rs.close();
	    			pst.close();
	    			if(leaderID != -1){
	    				pst = QuickQueue.conn.prepareStatement("UPDATE parties SET leaderID = '" + leaderID + "' WHERE partyID = '" + partyID + "'");
	    				pst.executeUpdate();
	    				pst.close();
	    				InventoryAPI.setPlayerInventoryType(sender, 1);
	    				sender.sendMessage(ChatColor.GOLD + "You successfully created a new party!");
	    				BadgeAPI.setPlayerBadge(sender.getUniqueId(), QuickQueue.getPlugin().getName(), "Party Animal", BadgeAPI.getPlayerBadgeProgress(sender.getUniqueId(),QuickQueue.getPlugin().getName(), "Party Animal")+1);
	    			}
    			}
    		}else{
    			sender.sendMessage(ChatColor.RED + "You can not create a party when you are already in one.");
    		}
    		
    		
    	}catch(Exception e){
    		e.printStackTrace();
    		sender.sendMessage(ChatColor.RED + "An error occurred while creating a new party.");
    	}
    }

    public void removeParty(Player sender) {
    	String senderUUID = sender.getUniqueId().toString();
    		int partyID = QueueAPI.getPartyIDByLeaderID(QueueAPI.getMemberID(senderUUID));
    		if(partyID != -1){//check sender is leader based on prior check
    			if(QueueAPI.destroyParty(partyID, sender.getUniqueId().toString())){
    				InventoryAPI.setPlayerInventoryType(sender, 0);//Hard setting as determining outcome is unneccesary
    				sender.sendMessage(ChatColor.GOLD + "Your party was successfully disbanded.");
    			}
    		}else{
    			sender.sendMessage(ChatColor.RED + "You must be a leader of a party to delete a party.");
    		}
    }
    
    public void leaveParty(Player sender) {
    	String senderUUID = sender.getUniqueId().toString();
		int memberID = QueueAPI.getMemberID(senderUUID);
		if(memberID != -1){//check sender is in a party
    		int partyID = QueueAPI.getPartyIDByLeaderID(memberID);
    		if(partyID == -1){//check sender is not leading a party
	    		QueueAPI.removePlayerFromParty(memberID);
	    		
	    		ArrayList<String> member = new ArrayList<String>();
				member.add(senderUUID);
				QueueAPI.removeListFromQueueBossBars(member);
				
	    		QueueAPI.applyAppropriateInventoryToPlayer(sender);
	    		sender.sendMessage(ChatColor.GOLD + "You have left your party.");
    		}else{
    			sender.sendMessage(ChatColor.RED + "You must disband your party in to leave it.");
    		}
		}else{
			sender.sendMessage(ChatColor.RED + "You are not in a party.");
		}
    }
    
    public void removeMember(Player sender, String[] args) {//arg1 = target player UUID
    	String senderUUID = sender.getUniqueId().toString();
    	String targetUUID = args[1];
    	int targetID = QueueAPI.getMemberID(targetUUID);
    	int senderID = QueueAPI.getMemberID(senderUUID);
    	if(senderID != targetID){//check sender is not removing themself
	    	int senderPartyID = QueueAPI.getPartyIDByLeaderID(senderID);
	    	int targetPartyID = QueueAPI.getMemberPartyID(targetID);
	    	if(senderPartyID != -1){//check sender is the leader of a party 
	    		if(targetPartyID == senderPartyID){//check target is in same party
	    			QueueAPI.removePlayerFromParty(targetID);
	    			
	    			ArrayList<String> member = new ArrayList<String>();
					member.add(targetUUID);
					QueueAPI.removeListFromQueueBossBars(member);
					
					QueueAPI.messageUUID(targetUUID, "1:", true);
	    		}else{
	    			sender.sendMessage(ChatColor.RED + "You cannot remove members of other parties.");
	    		}
	    	}else{
	    		sender.sendMessage(ChatColor.RED + "You must be a party leader to remove party members.");
	    	}
    	}else{
    		sender.sendMessage(ChatColor.RED + "You cannot remove yourself from a party. Either leave or disband your party.");
    	}
    }
    
    public void inviteMember(Player sender, String[] args) {//arg1 = invited player UUID
    	String senderUUID = sender.getUniqueId().toString();
    	String targetUUID = args[1];
    	int senderID = QueueAPI.getMemberID(senderUUID);
    	int targetID = QueueAPI.getMemberID(targetUUID);
    	if(QueueAPI.isPlayerOnline(targetUUID)){
	    	if(!senderUUID.equals(targetUUID)){//check sender is not inviting themself
		    	int senderPartyID = QueueAPI.getPartyIDByLeaderID(senderID);
		    	int senderGerneralPartyID = QueueAPI.getMemberPartyID(senderID);
		    	if(senderPartyID != -1 || senderGerneralPartyID != -1 && QueueAPI.memberInvitingEnabled(senderGerneralPartyID)){//check sender is the leader of a party or party of sender has memberInviting enabled
		    		if(targetID == -1){//check target is not already in a party
		    			if(!QueueAPI.inviteExists(targetUUID, senderUUID)){//check target has not already been invited to sender's party
				    		try{
				    		    PreparedStatement pst = QuickQueue.conn.prepareStatement("INSERT INTO invites (partyID, inviteRecipientUUID) VALUES ('" + senderGerneralPartyID + "', '" + targetUUID + "')");
				    		    pst.executeUpdate();
				    		    pst.close();	
				    		    
				    		    Player partyLeader = QuickQueue.getPlugin().getServer().getPlayer(UUID.fromString(QueueAPI.getPartyLeaderUUID(senderGerneralPartyID)));
				    			sender.sendMessage(ChatColor.GOLD + "Invite sent to " + ChatColor.GREEN + IdentifierAPI.getOnlineUserName(targetUUID) + ChatColor.GOLD + "!");
				    			QueueAPI.messageUUID(targetUUID, ChatColor.GOLD + "You recieved an invite to join " + ChatColor.GREEN + partyLeader.getDisplayName() + ChatColor.GOLD + "'s party!", false);
				        	}catch(Exception e){
				        		e.printStackTrace();
				        		sender.sendMessage(ChatColor.RED + "Failed to send invite.");
				        	}
		    			}else{
				    		sender.sendMessage(ChatColor.RED + "You have already sent an invite to this player.");
				    	}
		    		}else{
			    		sender.sendMessage(ChatColor.RED + "The player you are trying to invite is already in a party.");
			    	}
		    	}else{
		    		sender.sendMessage(ChatColor.RED + "You must be a party leader to send invites.");
		    	}
	    	}else{
	    		sender.sendMessage(ChatColor.RED + "You cannot invite yourself to your own party.");
			}
	    }else{
		    sender.sendMessage(ChatColor.RED + "This player is not online.");
		}
    }
    
    public void transferLeadership(Player sender, String[] args) {//arg1 = new leader UUID
    	String senderUUID = sender.getUniqueId().toString();
    	String targetUUID = args[1];
    	int senderID = QueueAPI.getMemberID(senderUUID);
    	int targetID = QueueAPI.getMemberID(targetUUID);
    	if(QueueAPI.isPlayerOnline(targetUUID)){
	    	if(!senderUUID.equals(targetUUID)){//check sender is not transferring to themself
		    	int senderPartyID = QueueAPI.getPartyIDByLeaderID(senderID);
		    	if(senderPartyID != -1){//check sender is the leader of a party 
		    		int targetPartyID = QueueAPI.getMemberPartyID(targetID);
		    		if(targetPartyID == senderPartyID){//check target is in same party
				    	try{
				    	    PreparedStatement pst = QuickQueue.conn.prepareStatement("UPDATE parties SET leaderID = '" + targetID + "' WHERE partyID = '" + senderPartyID + "'");
				    	    pst.executeUpdate();
				    	    pst.close();	
				    	    QueueAPI.applyAppropriateInventoryToPlayer(sender);
				    	    String newLeaderName = IdentifierAPI.getOnlineUserName(targetUUID);
				    	    sender.sendMessage(ChatColor.GOLD + "You made " +  ChatColor.GREEN + newLeaderName + ChatColor.GOLD + " the leader of your party.");
				    	    QueueAPI.messagePartyExcludeSender(senderPartyID, ChatColor.GREEN + newLeaderName + ChatColor.GOLD + " is now the party leader.", false, targetUUID);
				    	    QueueAPI.messageUUID(targetUUID, "1:", true);
				    	    QueueAPI.messageUUID(targetUUID, ChatColor.GOLD + "You were made party leader!", false);
				        }catch(Exception e){
				        	e.printStackTrace();
				        	sender.sendMessage(ChatColor.RED + "Failed to send invite.");
				        }
		    		}else{
			    		sender.sendMessage(ChatColor.RED + "That player is not in your party.");
			    	}
		    	}else{
		    		sender.sendMessage(ChatColor.RED + "You must be a party leader to transfer leadership.");
		    	}
	    	}else{
	    		sender.sendMessage(ChatColor.RED + "You are already the leader of this party.");
			}
	    }else{
		    sender.sendMessage(ChatColor.RED + "This player is not online.");
		}
    }
    
    public void revokeInvite(Player sender, String[] args) {//arg1 = invited UUID
    	String senderUUID = sender.getUniqueId().toString();
    	String targetUUID = args[1];
    	int senderID = QueueAPI.getMemberID(senderUUID);
    	int senderPartyID = QueueAPI.getPartyIDByLeaderID(senderID);
    	int senderGerneralPartyID = QueueAPI.getMemberPartyID(senderID);
    	if(QueueAPI.isPlayerOnline(targetUUID)){
	    	if(senderPartyID != -1 || senderGerneralPartyID != -1 && QueueAPI.memberInvitingEnabled(senderGerneralPartyID)){//check sender is the leader of a party or party of sender has memberInviting enabled
				if(QueueAPI.inviteExists(targetUUID, senderUUID)){//check target has not already been invited to sender's party
		    		try{
		    		    PreparedStatement pst = QuickQueue.conn.prepareStatement("DELETE FROM invites WHERE partyID = '" + senderGerneralPartyID + "' AND inviteRecipientUUID = '" + targetUUID + "'");
		    		    pst.executeUpdate();
		    		    pst.close();	
		    		    
		    			sender.sendMessage(ChatColor.GOLD + "Invite revoked.");
		        	}catch(Exception e){
		        		e.printStackTrace();
		        		sender.sendMessage(ChatColor.RED + "Failed to send invite.");
		        	}
				}else{
		    		sender.sendMessage(ChatColor.RED + "You have already sent an invite to this player.");
		    	}
	    	}else{
	    		sender.sendMessage(ChatColor.RED + "You must be a party leader to revoke invites.");
	    	}
	    }else{
		    sender.sendMessage(ChatColor.RED + "This player is not online.");
		}
    }
    
    public void acceptInvite(Player sender, String[] args) {//arg1 = inviter UUID
    	String sourceUUID = args[1];
    	String targetUUID = sender.getUniqueId().toString();
    	if(QueueAPI.isPlayerOnline(targetUUID)){
	    	if(QueueAPI.getMemberID(targetUUID) == -1){//check user is not already in a party
		    	if(QueueAPI.inviteExists(targetUUID, sourceUUID)){//check the invite is still valid
		    		try{
		    			int partyID = QueueAPI.getMemberPartyID(QueueAPI.getMemberID(sourceUUID));
		    		    PreparedStatement pst = QuickQueue.conn.prepareStatement("INSERT INTO members (memberUUID, partyID) VALUES ('" + targetUUID + "', '" + partyID + "')");
		    		    pst.executeUpdate();
		    		    pst.close();	
		    		    
		    			sender.sendMessage(ChatColor.GOLD + "You joined " + ChatColor.GREEN + IdentifierAPI.getOnlineUserName(sourceUUID) + ChatColor.GOLD + "'s party!");
		    			if(QueueAPI.memberInvitingEnabled(partyID)){
		    				InventoryAPI.setPlayerInventoryType(sender, 3);
		    			}else{
		    				InventoryAPI.setPlayerInventoryType(sender, 2);
		    			}
		    			QueueAPI.messagePartyExcludeSender(partyID, ChatColor.GREEN + sender.getDisplayName() + ChatColor.GOLD + " joined your party!", false, targetUUID);
		    			QueueAPI.addUUIDToQueueBossBars(targetUUID, QueueAPI.getQueuedMinigame(partyID));
		    			
		    			BadgeAPI.setPlayerBadge(sender.getUniqueId(), QuickQueue.getPlugin().getName(), "Party Animal", BadgeAPI.getPlayerBadgeProgress(sender.getUniqueId(),QuickQueue.getPlugin().getName(), "Party Animal")+1);
		        	}catch(Exception e){
		        		e.printStackTrace();
		        		sender.sendMessage(ChatColor.RED + "Failed to join party.");
		        	}
				}else{
					sender.sendMessage(ChatColor.RED + "You do not have an invite to that party.");
				}
	    	}else{
	    		sender.sendMessage(ChatColor.RED + "You cannot accept invites whilst in a party.");
	    	}
	    	
	    	QueueAPI.destroyInvites(targetUUID);
	    }else{
		    sender.sendMessage(ChatColor.RED + "This player is not online.");
		}
    }
    
    public void toggleMemberInviting(Player sender) {
    	UUID fullUUID = sender.getUniqueId();
    	long timeRemainingToReuse = QuickQueue.memberInvitingToggleUsage.get(fullUUID) + (QuickQueue.getConfigModel().memberInvitingToggleDelay) - System.currentTimeMillis();
    	if(timeRemainingToReuse <= 0){
    		QuickQueue.memberInvitingToggleUsage.replace(fullUUID, System.currentTimeMillis());
	    	final String senderUUID = sender.getUniqueId().toString();
	    	final int senderID = QueueAPI.getMemberID(senderUUID);
	    	final int senderPartyID = QueueAPI.getPartyIDByLeaderID(senderID);
	    	final String sql = "UPDATE parties SET memberInviting = (NOT memberInviting) WHERE partyID = '" + senderPartyID + "'";
	    	new BukkitRunnable() {
	            @Override
	            public void run() {
			    	int senderID = QueueAPI.getMemberID(senderUUID);
			    	int senderPartyID = QueueAPI.getPartyIDByLeaderID(senderID);
			    	if(senderPartyID != -1){//check sender is the leader of a party
			    		try (Connection conn = QuickQueue.sqlPool.getConnection(); PreparedStatement pst = conn.prepareStatement(sql)){
				    	    pst.executeUpdate();
				    	    
				    	    boolean memberInvitingEnabled = QueueAPI.memberInvitingEnabled(senderPartyID);
				    	    String stateString = ChatColor.GREEN + "enabled";
				    	    if(!memberInvitingEnabled){
			    	    		stateString = ChatColor.RED + "disabled";
				    	    }
				    	    QueueAPI.messageParty(senderPartyID, ChatColor.GOLD + "Member invite privileges were " + stateString + ChatColor.GOLD + " for your party.", false);
				    	    QueueAPI.messageParty(senderPartyID, "1:", true);
				    	    QueueAPI.messageUUID(senderUUID, "4:", true);
				    		pst.close();
				    		
				        }catch(Exception e){
				        	e.printStackTrace();
				        	sender.sendMessage(ChatColor.RED + "Failed to set invite privileges.");
				        }
			    	}else{
			    		sender.sendMessage(ChatColor.RED + "You must be a party leader to toggle member invite privileges.");
			    	}
	            }
	    	}.runTaskAsynchronously(QuickQueue.getPlugin());
    	}else{
    		sender.sendMessage(ChatColor.RED + "Please wait another " + (timeRemainingToReuse/1000+1) + " seconds before using this again.");
    	}
    }

    public void joinQueue(Player sender, String[] args) { //arg1 = minigame ID
    	String senderUUID = sender.getUniqueId().toString();
    	int gameID;
    	try{
    		gameID = Integer.parseInt(args[1]);
    	}catch(Exception e){
    		e.printStackTrace();
    		sender.sendMessage(ChatColor.RED + "That minigame does not exist.");
    		return;
    	}
    	int partyID = QueueAPI.getPartyIDByLeaderID(QueueAPI.getMemberID(senderUUID));
		if(partyID != -1){//check sender is leader based on prior check
	    	if(QueueAPI.isMinigameQueueable(gameID)){
	    		if(QueueAPI.checkMinigameInBeta(gameID, partyID)){
			    	if(QueueAPI.isMinigameAccessible(gameID)){//Is game public/existent
			    		try{//TODO get actual queue id rather than game id
				    		PreparedStatement pst = QuickQueue.conn.prepareStatement("UPDATE parties SET queueID = '" + gameID + "', isQueueing = '1' WHERE partyID = '" + partyID + "'");
				    		pst.executeUpdate();
				    		pst.close();
				    		String gameName = QueueAPI.getMinigameName(gameID);
				    		String[] members = QueueAPI.messageParty(partyID, ChatColor.GOLD + "Your party has queued for " + ChatColor.GREEN + gameName + ChatColor.GOLD + ".", false);
				    		
				    		QueueAPI.addListToQueueBossBars(members, gameID);
				    	}catch(Exception e){
				    		e.printStackTrace();
				    		sender.sendMessage(ChatColor.RED + "Failed to join the queue.");
				    	}
			    	}else{
						sender.sendMessage(ChatColor.RED + "That minigame does not exist.");
					}
		    	}else{
					sender.sendMessage(ChatColor.RED + "You cannot connect to this minigame as all members of your party must be premium.");
				}
	    	}else{
	    		try (Jedis jedis = QuickQueue.pool.getResource()){
	    			jedis.set("instantqueue:" + partyID, String.valueOf(gameID));
	    			jedis.sadd("instantqueues", "instantqueue:" + partyID);
	    		}
	    	}
		}else{
    		sender.sendMessage(ChatColor.RED + "You must be a leader of a party to join a queue.");
    	}
    }
	
    public void exitQueue(Player sender) {
    	String senderUUID = sender.getUniqueId().toString();
    	
    	try{
    		int partyID = QueueAPI.getPartyIDByLeaderID(QueueAPI.getMemberID(senderUUID));
    		if(partyID != -1){//check sender is leader based on prior check
    			if(QueueAPI.isPartyQueued(partyID)){//check party is actually queued//TODO error protection
		    		PreparedStatement pst = QuickQueue.conn.prepareStatement("UPDATE parties SET queueID = NULL, isQueueing = '0' WHERE partyID = '" + partyID + "'");
		    		pst.executeUpdate();
		    		pst.close();
		    		
		    		ArrayList<String> members = QueueAPI.getPartyMembers(partyID);
		    		QueueAPI.removeListFromQueueBossBars(members);
		    		
		    		QueueAPI.messageParty(partyID, ChatColor.GOLD + "Your party has exited the queue.", false);
    			}else{
    				sender.sendMessage(ChatColor.RED + "Your party is not queued.");
    			}
    		}else{
    			sender.sendMessage(ChatColor.RED + "You must be a leader of a party to exit the queue.");
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
}
