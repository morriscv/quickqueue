package network.marble.quickqueue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.inventivetalent.bossbar.BossBar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import net.md_5.bungee.api.ChatColor;
import network.marble.badgeapi.api.BadgeAPI;
import network.marble.bossbarmanager.IdentifiablePluginString;
import network.marble.bossbarmanager.PluginString;
import network.marble.bossbarmanager.api.BossBarManagerAPI;
import network.marble.messageapi.api.MessageAPI;
import network.marble.quickqueue.tasks.MessageCollector;
import network.marble.quickqueue.api.QQMenus;
import network.marble.quickqueue.api.QueueAPI;
import network.marble.quickqueue.api.QuickQueueMessages;
import network.marble.quickqueue.commands.QueueCommands;
import network.marble.quickqueue.listeners.QueueListener;
import network.marble.serveridentifier.api.IdentifierAPI;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class QuickQueue extends JavaPlugin {
    private static QuickQueue plugin;
    private static QueueConfig config;
    public static final QuickQueueMessages messageClass = new QuickQueueMessages();
    public static Connection conn = null;
    public static HikariDataSource sqlPool;
    public static JedisPool pool;
    public static long serverID;
    
    public static Map<UUID, Long> memberInvitingToggleUsage = new HashMap<UUID, Long>();
    public static Map<Player, BossBar> bossBars = new HashMap<Player, BossBar>();
    
    @Override
    public void onEnable() {
    	if(isDataManagerPresent()){
	        plugin = this;
	        initConfig(false);
	        registerCommands();
	        setupJedisPool();
	        getServerID();
	        registerEvents();
	        registerMessageExecutor();
	        prepareMySQL();
	        QQMenus.buildInventoryMenus();
	        registerBossBarManager();
	        buildInventoryTypes();//In place to handle loss of data in reloads//TODO handle player bossbars
	        activateSchedule();
	        registerBadges();
	        getLogger().info("QuickQueue successfully loaded.");
    	}else{
    		getLogger().severe("Data Manager is not present. Disabling.");
    		setEnabled(false);
    	}
    }
    
	@Override
    public void onDisable(){
		for(Player p: this.getServer().getOnlinePlayers()){
			QueueAPI.markPlayerOffline(p);
		}
    	try {
    		if(!conn.equals(null)){
    			conn.close();
    		}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	pool.destroy();
    }
    
    private void setupJedisPool(){
    	try{
    		JedisPoolConfig poolcfg = new JedisPoolConfig();
	        poolcfg.setMaxTotal(getServer().getMaxPlayers() + 1);//Everyone can disconnect simultaneously
	        pool = new JedisPool(new JedisPoolConfig(), config.redisHost, config.redisPort);
    	}catch(Exception e){
    		getLogger().severe("Failed to load at Redis setup.");
            getLogger().severe(e.getMessage());
    		setEnabled(false);
    	}
    }

    private void initConfig(boolean repeat) {
    	try(Reader reader = new FileReader(this.getDataFolder() + "/config.json")){
			config = new Gson().fromJson(reader, QueueConfig.class);
    		return;
		} catch (Exception e){
			getLogger().info("Generating config.");
		}
    	QueueConfig builder = new QueueConfig(); 
    	File directory = this.getDataFolder();
		
    	if (directory.exists() == false){
			try{
				directory.mkdir();
			}catch(Exception e){
				if(repeat){
					this.setEnabled(false);
				}
			}
		}
		
		try(Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.getDataFolder() + "/config.json")))){
			Gson file = new GsonBuilder().create();
			file.toJson(builder, writer);
			getLogger().info("Config file has been created");
		} catch (Exception e){
			e.printStackTrace();
			if(repeat){
				this.setEnabled(false);
			}
		}
		if(!repeat){
			initConfig(true);
		}
	}
    
    private void getServerID(){
    	serverID = IdentifierAPI.getServerID();
    }
    
    private void registerCommands() {
    	getCommand("qq").setExecutor(new QueueCommands());
    }
    
    private void registerEvents(){
    	getServer().getPluginManager().registerEvents(new QueueListener(), this);
    }
    
    private void registerMessageExecutor(){
    	MessageAPI.registerPlugin(this.getName(), messageClass);
    }
    
    private void prepareMySQL(){
    	try{
    		conn = DriverManager.getConnection("jdbc:mysql://" + QuickQueue.config.sqlHost + ":" + QuickQueue.config.sqlPort + "/" + QuickQueue.config.database, QuickQueue.config.sqlUsername, QuickQueue.config.sqlPassword);
    		
    		HikariConfig config = new HikariConfig();
        	config.setJdbcUrl("jdbc:mysql://" + QuickQueue.config.sqlHost + ":" + QuickQueue.config.sqlPort + "/" + QuickQueue.config.database);
        	config.setUsername(QuickQueue.config.sqlUsername);
        	config.setPassword(QuickQueue.config.sqlPassword);
        	config.setConnectionTimeout(5000);
        	//TODO consider modifying time-outs and cache sizes

        	sqlPool = new HikariDataSource(config);
    	}catch (SQLException e){
    		getLogger().severe("Failed to load at MySQL connection initialisation.");
            getLogger().severe(e.getMessage());
            setEnabled(false);
    	}
    }
    
    private void registerBossBarManager() {
    	try(Jedis jedis = pool.getResource()){
    		ArrayList<IdentifiablePluginString> strings = new ArrayList<IdentifiablePluginString>();
    		BossBarManagerAPI.registerPlugin(getName(), 100);
    		
		    PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT minigameID, minigameName FROM minigames WHERE isPublic = '1' AND isQueueable = '1'");
		    ResultSet rs = pst.executeQuery();
		    while(rs.next()){
		    	int gameID = rs.getInt("minigameID");
		    	String key = "gameavgwait:" + gameID;
		    	
		    	String string = ChatColor.GOLD + "Approximate wait for ";
		    	string += ChatColor.GREEN + rs.getString("minigameName");
		    	string += ChatColor.GOLD + ": ";
		    	if(jedis.exists(key)){
			    	String avgTimeString = jedis.get(key);
			    	String finalAvgTimeString;
			    	
			    	double waitSeconds = Integer.valueOf(avgTimeString);
			    	if(waitSeconds>0){
			    		double waitMinutes = Math.ceil(waitSeconds/60);
			    		finalAvgTimeString = "\u2248" + (int)waitMinutes + " minute";
			    		if((int)waitMinutes > 1){
			    			finalAvgTimeString += "s";
			    		}
			    		finalAvgTimeString += ".";
			    	}else{
			    		finalAvgTimeString = "nearly instant.";
			    	}
			    	string += finalAvgTimeString;
			    	
		    	}else{
		    		string += "calculating...";
		    	}
		    	strings.add(new IdentifiablePluginString(gameID, new PluginString(getName(), string)));
		    }
		    
		    BossBarManagerAPI.updateStrings(getName(), strings);
    	}catch(Exception e){
    		getLogger().severe("Failed to load at BBM registration.");
    		e.printStackTrace();
    		this.setEnabled(false);
    	}
		
	}

	private void buildInventoryTypes() {
    	for (Player player : getServer().getOnlinePlayers()) {
            QueueAPI.applyAppropriateInventoryToPlayer(player);
        }
	}
	
	private void activateSchedule() {
		BukkitScheduler scheduler = getServer().getScheduler();
		 scheduler.scheduleSyncRepeatingTask(this, new MessageCollector(), 0L, 20*config.checkRate);
	}
	
	private void registerBadges() {
		BadgeAPI.createBadgeIfNotExists(getName(), "Party Animal", "Join or create 100 parties.", 100, false, false);
	}
    
    public static QuickQueue getPlugin() {
        return plugin;
    }
    
    public static QuickQueueMessages getMessageClass() {
    	return messageClass;
    }
    
    public static QueueConfig getConfigModel() {
        return config;
    }

    public static boolean isDataManagerPresent(){ 
        return Bukkit.getPluginManager().getPlugin("MRN-DataStorageManager") != null;
    } //Return true if the DataStorageManager is found, and therefore it is safe to use functionality from it, or false otherwise.
    
}
