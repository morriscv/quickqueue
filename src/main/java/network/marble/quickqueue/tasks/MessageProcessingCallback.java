package network.marble.quickqueue.tasks;

import java.util.ArrayList;
import network.marble.bossbarmanager.IdentifiablePluginString;

public interface MessageProcessingCallback {
	
	public void onPlayerMessagesProcessed(ArrayList<IdentifiablePluginString> strings);
	
}
