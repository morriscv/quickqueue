package network.marble.quickqueue.tasks;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.bukkit.Bukkit;

import net.md_5.bungee.api.ChatColor;
import network.marble.bossbarmanager.IdentifiablePluginString;
import network.marble.bossbarmanager.PluginString;
import network.marble.bossbarmanager.api.BossBarManagerAPI;
import network.marble.quickqueue.QuickQueue;
import network.marble.serveridentifier.api.IdentifierAPI;
import redis.clients.jedis.Jedis;

public class MessageCollector implements Runnable{
	
	@Override
	public void run() {
		
		getMessages(String.valueOf(IdentifierAPI.getServerID()), new MessageProcessingCallback(){
			@Override
			public void onPlayerMessagesProcessed(ArrayList<IdentifiablePluginString> strings){
				if(strings != null){
					BossBarManagerAPI.updateStrings(QuickQueue.getPlugin().getName(), strings);
				}
			}
		});
	}
	
	public static void getMessages(final String serverID, final MessageProcessingCallback callback){
        Bukkit.getScheduler().runTaskAsynchronously(QuickQueue.getPlugin(), new Runnable(){
        	@Override
        	public void run(){
        		ArrayList<IdentifiablePluginString> strings = new ArrayList<IdentifiablePluginString>();
        		try(Jedis jedis = QuickQueue.pool.getResource()){
        		    PreparedStatement pst = QuickQueue.conn.prepareStatement("SELECT minigameID, minigameName FROM minigames WHERE isPublic = '1' AND isQueueable = '1'");
        		    ResultSet rs = pst.executeQuery();
        		    while(rs.next()){
        		    	int gameID = rs.getInt("minigameID");
        		    	String key = "gameavgwait:" + gameID;
        		    	
        		    	String string = ChatColor.GOLD + "Approximate wait for ";
        		    	string += ChatColor.GREEN + rs.getString("minigameName");
        		    	string += ChatColor.GOLD + ": ";
        		    	if(jedis.exists(key)){
        			    	String avgTimeString = jedis.get(key);
        			    	String finalAvgTimeString;
        			    	
        			    	double waitSeconds = Integer.valueOf(avgTimeString);
        			    	if(waitSeconds>0){
        			    		double waitMinutes = Math.ceil(waitSeconds/60);
        			    		finalAvgTimeString = "\u2248" + (int)waitMinutes + " minute";
        			    		if((int)waitMinutes > 1){
        			    			finalAvgTimeString += "s";
        			    		}
        			    		finalAvgTimeString += ".";
        			    	}else{
        			    		finalAvgTimeString = "nearly instant.";
        			    	}
        			    	string += finalAvgTimeString;
        			    	
        		    	}else{
        		    		string += "calculating...";
        		    	}
        		    	strings.add(new IdentifiablePluginString(gameID, new PluginString(QuickQueue.getPlugin().getName(), string)));
        		    }
        		    
        		    
            	}catch(Exception e){
            		e.printStackTrace();
            	}
        		
        	
        		
                Bukkit.getScheduler().runTask(QuickQueue.getPlugin(), new Runnable(){
                    @Override
                    public void run(){
                        callback.onPlayerMessagesProcessed(strings);
                    }
                });
            }
        });
    }

}
