package network.marble.quickqueue;

public class QueueConfig {
	public String redisHost = "127.0.0.1";
    public int redisPort = 6379;
	public String sqlHost = "178.32.10.184";
    public int sqlPort = 3306;
    public String sqlUsername = "QueuePlugin";
    public String sqlPassword = "todoSecurity";
    public String database = "MarbleNetwork_Queues";
    public int memberInvitingToggleDelay = 15*1000;
    public int checkRate = 10;

	public QueueConfig() {
	}
}
