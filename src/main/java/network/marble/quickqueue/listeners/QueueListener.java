package network.marble.quickqueue.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import network.marble.quickqueue.QuickQueue;
import network.marble.quickqueue.api.QueueAPI;

public class QueueListener implements Listener {

	@EventHandler(priority = EventPriority.HIGH)
	public void onLogin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		QuickQueue.memberInvitingToggleUsage.putIfAbsent(event.getPlayer().getUniqueId(), 0L);
		QueueAPI.applyAppropriateInventoryToPlayer(player);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onDisconnect(PlayerQuitEvent event) {
		QuickQueue.memberInvitingToggleUsage.remove(event.getPlayer().getUniqueId());
		QueueAPI.markPlayerOffline(event.getPlayer());
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onKick(PlayerKickEvent event) {
		QueueAPI.markPlayerOffline(event.getPlayer());
	}
	
	
}
